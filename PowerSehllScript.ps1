#List services thats are status is enabled/running
Get-Command -Name *service*
Get-Help -Name Get-Service
Get-Service | Select-Object -Property Status
Get-Service| Select-Object -Property Status -Unique
Get-Service | Where-Object {$PsItem.Status -eq "Running"}

#Startype Automatic ve Manuel olanları listele
Get-Service | Where-Object {$PsItem.StartType -eq "Automatic","Manual"} |Sort-Object -Property Name


#Startype Automatic olan Delayed Start
Get-Service | Get-Member "StartType"
Get-Service ALG|Select-Object -Property StartType
Get-Service | Select-Object -Property StartType -Unique
Get-Service| Select-Object -Property State, Name, DisplayName, StartType
Set-Service -Name "ALG" –StartupType "Automatic (Delayed Start)"
$PSVersionTable

Get-Service | Where-Object {$PsItem.StartType -eq "AutomaticDelayedStart"}


#Disk boyutlarını html formatında rapoprlayan ve mail atanGet-Disk scr
Get-Volume | Sort-Object -Property Size | Out-File -FilePath C:\TBB\Deneme2.pdf
$From = "kllcbusra13@gmail.com"
$To = "kllcbusra13@gmail.com"
$Attachment = "C:\TBB\Deneme2.pdf"
$Subject = "Email Subject"
$Body = "Insert body text here"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"
Send-MailMessage -From $From -to $To -Subject $Subject `
-Body $Body -SmtpServer $SMTPServer -port $SMTPPort -UseSsl `
-Credential (Get-Credential) -Attachments $Attachment




#apı üzerinde get ve post işlemleri
$sp=Invoke-RestMethod -Method GET -Uri http://api.open-notify.org/astros.json
$sp.people
#Post İşlemi
$Comments = @{
    name= "Busra Kilic";
    email="buskilic@example.com";
    body="TBB HalfTerm Project POST Request Example";
}
$Json = $Comments | ConvertTo-Json
Invoke-RestMethod -Method Post -Uri "http://jsonplaceholder.typicode.com/comments" -Body $Json -ContentType 'application/json'
